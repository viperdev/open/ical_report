#! /usr/bin/python3

from configparser import ConfigParser
from argparse import ArgumentParser
from datetime import timedelta, datetime

import requests
from ics import Calendar


def parse_args():
    parser = ArgumentParser(
        description="Creates a report for your week from your calendar.",
    )
    parser.add_argument('url', help='your iCal URL')

    return parser.parse_args()


def main():
    args = parse_args()

    today = datetime.now().date()
    last_monday = today - timedelta(days=today.weekday())
    next_monday = today - timedelta(days=today.weekday()-7)

    url = args.url
    calendar = Calendar(requests.get(url).text)
    print(f'Showing events between  { last_monday } and { next_monday }')
    print()

    total_length = timedelta(0)
    for event in calendar.events:
        if last_monday < event.begin.date() < next_monday:
            total_length += event.end - event.begin
            print(event.begin, event.name)

    print()
    print(f'You have worked { total_length.total_seconds()/60/60 } hours this week.')


if __name__ == '__main__':
    main()
